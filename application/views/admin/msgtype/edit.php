<div id="win_r">
    <form action="<?php echo site_url('admin/msgtype_doedit'); ?>" method="post">
        <ul>
            <li>
                <span class="title"><?php echo lang('msg_type'); ?></span>
                <input type="hidden" name="id" value="<?php echo $msgtype->id; ?>" />
                <input type="text" name="type_name" class="input" value="<?php echo $msgtype->type_name; ?>" maxlength="10" />
                <span class="m_left_10 notice"><?php echo lang('msg_type_note'); ?></span>
            </li>
            <li>
                <span class="title"><?php echo lang('msg_type_desc'); ?></span>
                <textarea name="description" class="input_area" maxlength="100"><?php echo $msgtype->description; ?></textarea>
            </li>
            <li>
                <span class="title"><?php echo lang('msg_type_for_reply'); ?></span>
                <select class="input" name="for_reply">
                    <option value="0"><?php echo lang('no'); ?></option>
                    <option value="1" <?php echo ($msgtype->for_reply ? 'selected="selected"' : ''); ?>><?php echo lang('yes'); ?></option>
                </select>
                <span class="m_left_10 notice"><?php echo lang('msg_type_for_reply_note'); ?></span>
            </li>
            <li class="text_c">
                <input type="submit" value="<?php echo lang('edit'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>