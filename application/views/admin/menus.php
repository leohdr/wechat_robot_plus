<div id="win_l">
    <ul id="admin_menu">
        <li onclick="togglemenu('keywords_panel');">
            <a href="javascript:void(0);"><?php echo lang('keywords_manage'); ?></a>
            <ul class="keywords_panel">
                <li><?php echo anchor('admin/keywords_index', lang('keywords_list')); ?></li>
                <li><?php echo anchor('admin/keywords_add', lang('add_keyword')); ?></li>
            </ul>
        </li>
        <li><hr /></li>
        <li onclick="togglemenu('reply_panel');">
            <a href="javascript:void(0);"><?php echo lang('reply_manage'); ?></a>
            <ul class="reply_panel">
                <li><?php echo anchor('admin/reply_index', lang('reply_list')); ?></li>
                <li><?php echo anchor('admin/reply_add', lang('add_reply')); ?></li>
            </ul>
        </li>
        <li><hr /></li>
        <li onclick="togglemenu('commands_panel');">
            <a href="javascript:void(0);"><?php echo lang('commands_manage'); ?></a>
            <ul class="commands_panel">
                <li><?php echo anchor('admin/commands_index', lang('commands_list')); ?></li>
                <li><?php echo anchor('admin/commands_add', lang('add_command')); ?></li>
            </ul>
        </li>
        <li><hr /></li>
        <li onclick="togglemenu('msgtype_panel');">
            <a href="javascript:void(0);"><?php echo lang('msgtype_manage'); ?></a>
            <ul class="msgtype_panel">
                <li><?php echo anchor('admin/msgtype_index', lang('msgtype_list')); ?></li>
                <li><?php echo anchor('admin/msgtype_add', lang('add_msgtype')); ?></li>
            </ul>
        </li>
        <li><hr /></li>
        <li onclick="togglemenu('user_panel');">
            <a href="javascript:void(0);"><?php echo lang('user_manage'); ?></a>
            <ul class="user_panel">
                <li><?php echo anchor('admin/user_index', lang('users_list')); ?></li>
                <li><?php echo anchor('admin/user_add', lang('add_user')); ?></li>
            </ul>
        </li>
        <li><hr /></li>
        <li onclick="togglemenu('blacklists_panel');">
            <a href="javascript:void(0);"><?php echo lang('blacklists_manage'); ?></a>
            <ul class="blacklists_panel">
                <li><?php echo anchor('admin/blacklists_index', lang('blacklists_list')); ?></li>
                <li><?php echo anchor('admin/blacklists_add', lang('add_blacklist')); ?></li>
            </ul>
        </li>
    </ul>
</div>