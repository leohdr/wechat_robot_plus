function iframe_uploader(source_id, action_url, result_field_id) {
    var sourceForm = null;
    var iu = null;
    //创建或指定iframe
    if ($("#iframe_uploader").length > 0) {
        iu = $("#iframe_uploader");
        //清空iframe
        iu.contents().find("body").html("");
    } else {
        iu = $('<iframe id="iframe_uploader" style="position:absolute;top:-999px;left:-999px;height:0;width:0;display:none"></iframe>').appendTo($(document.body));
    }
    //创建一个form
    sourceForm = $('<form id="upload_form" action="' + action_url + '" method="post" enctype="multipart/form-data"></form>').appendTo(iu.contents().find("body"));
    //创建一个隐藏域提交接收返回文件名元素的Id
    $('<input type="hidden" name="result_field_id" value="' + result_field_id + '" />').appendTo(sourceForm);
    //移动源到iframe内并提交
    sourceForm.append($("#" + source_id)).submit();
}

//url跳转
function url_redirect(url) {
    window.location = url;
}

//菜单收缩
function togglemenu(menu) {
    $("." + menu).slideToggle("fast");
}

$(document).ready(function() {
    //当前行变色
    $("tr").hover(function() {
        $(this).addClass("this_row").siblings().removeClass("this_row");
    }, function() {
        $(this).removeClass("this_row");
    });
});